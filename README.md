# Campaigns

Team responsible for developing, executing, and optimizing demand generation campaigns.

See our handbook for more information: https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/