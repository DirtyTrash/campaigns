<!-- Purpose of this issue: To create a MURAL of the campaign to show the cohesive design and messaging and for full transparency by all teams. -->
<!-- This issue type is to be opened by members of the Campaigns Team. -->

## Action Items Checklist
* [ ] Name this issue `Campmaign MURAL: [name of campaign]` (ex. Campaign MURAL: GitOps Use Case Campaign)
* [ ] Set the due date of the `Integrated Campaign Journey Mural`, based on the [Integrated Campaign GANTT chart](https://docs.google.com/spreadsheets/d/1GFON-5vAcUqOAYaKcmK0Qm9tm-GThatIOD7W1_cn6Ic/edit#gid=1426779885)
* [ ] Update milestone appropriately
* [ ] Clone the [MURAL template](https://app.mural.co/t/gitlab5736/m/gitlab5736/1592576934641/7d210044a2794988ad855c3947278d2fb31587ad)
* [ ] Add new MURAL link here: 
* [ ] Add link in the following locations:
   - [ ] Main Integrated campaign Epic
   - [ ] SDR enablement Epic
   - [ ] Nurture Plan issue
* Close out this issue.


<!-- DO NOT UPDATE - PROECT MANAGEMENT
/milestone %
/assign @
/due 
/label ~"mktg-status::wip" ~"mktg-demandgen" ~"dg-campaigns"
-->
